always-enable-nxm-download.user.js
==================================

Userscript to always enable `nxm://` downloads for mod files on
[NexusMods.com](https://www.nexusmods.com/).

Motivation
----------

Most mods will have the “Mod manager download” download option enabled for all
their files. Most often the exceptions are when either:
1. The mod maintainer forgot or misunderstood the option and thus either
   elected to not select it or mistakenly unselected it. Some times contacting
   the mod maintainer will let them fix this, but many times they will also
   simply be unresponsive or still not understand what it’s for.
2. The files are not really useful in a mod manager context.
   E.g., 3rd party tools (such as
   [LOOT](https://www.nexusmods.com/skyrimspecialedition/mods/1918) or xEdit or
   [Mod Organizer](https://www.nexusmods.com/skyrimspecialedition/mods/6194)).
   In these cases it is often *not* a mistake on the mod page maintainers part
   to not enable mod manager downloads… But as a user, sometimes you want to
   download and manage your downloads via your mod manager anyway.

Usage and installation
----------------------

See the [Userscript Beginners HOWTO](https://openuserjs.org/about/Userscript-Beginners-HOWTO)
from [OpenUserJS](https://openuserjs.org/about/Userscript-Beginners-HOWTO) for
how to enable userscript in your browser in the first place.

Once you have installed your userscript manager of choice, proceed to
[install the `always-enable-nxm-download.user.js` userscript](https://gitlab.com/Freso/always-enable-nxm-download/raw/master/always-enable-nxm-download.user.js)
from here (if you installed your userscript manager correctly, clicking this
link should open a dialog asking to install the userscript).
Once the script’s installed, it should Just Work™.

Contact
-------

The project is hosted on GitLab, and any bugs, feature requests, etc.
should go there: <https://gitlab.com/Freso/always-enable-nxm-download>

You can also discuss this project on [Freenode IRC](https://freenode.net/) in
the [##Freso channel](https://webchat.freenode.net/?channels=##Freso), or
alternatively on [Discord](https://discord.gg/hU85Grw).

Acknowledgments
---------------

This userscript was made with the support of
[my patrons on Patreon](https://www.patreon.com/Freso).
Thank you so much for supporting my work with open data and free and
open source software!

License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

